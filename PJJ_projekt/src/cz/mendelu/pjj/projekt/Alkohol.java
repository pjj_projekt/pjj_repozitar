package cz.mendelu.pjj.projekt;

public class Alkohol extends Surovina
{

	private float kvalita;

	/**
	 * 
	 * @param kvalita
	 */
	public Alkohol(float kvalita, double cena, int velikostPolozky)
	{
		super(cena, velikostPolozky);
		this.kvalita = kvalita;
		this.typSuroviny = "alkohol";
	}

	public double getCena()
	{
		return cena * kvalita; // upravit pocitani
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setCena(double oKolik)
	{
		this.cena += oKolik;
	}

	public float getKvalita()
	{
		return this.kvalita;
	}

}