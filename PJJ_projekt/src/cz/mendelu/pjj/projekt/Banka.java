package cz.mendelu.pjj.projekt;

import java.util.LinkedList;

public class Banka extends Building
{

	private LinkedList<Ochranka> ochranka;
	private int maxPocetOchrankaru;

	/**
	 * 
	 * @param nazev
	 * @param pocetLidi
	 * @param cena
	 */
	public Banka(String nazev, int pocetLidi, double cena, int level, int maxPocetOchrankaru)
	{
		super(nazev, pocetLidi, cena, level);
		this.maxPocetOchrankaru = maxPocetOchrankaru;
		this.ochranka = new LinkedList();
		this.ochranka.clear();

	}

	/**
	 * 
	 * @param ochranka
	 */
	public void najmoutOchranku(Ochranka ochranka)
	{
		if (this.ochranka.size() < maxPocetOchrankaru)
		{
			this.ochranka.addLast(ochranka);
			// neco jako
			// hrac.setmoney(-ochranka.getVyplata());
			// nebo v kazdem kole
			// hrac.setmoney(-ochranka.getvyplata());
			// ===>> nebo jenom to odecist ze zisku budovi -> napr. zisk muze byt zaporny a hraci prichazi o penize
		}
		else
		{
			System.out.println("Nelze najmout vice ochrankaru");
		}
	}

	/**
	 * 
	 * @param kolik
	 */
	public void vzitSiPujcku(double kolik)
	{
		// hrac.setMoney(kolik);
		// ulozit do nejaky promenne dluh + nejaky pocitadlo kol, ktere bude zvetsovat urok
	}

	public void splatitPujcku()
	{
		// nejaka prommenna -> dluh = 0;
	}

	public int getMaxPocetOchrankaru()
	{
		return this.maxPocetOchrankaru;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setMaxPocetOchrankaru(int okolik)
	{
		this.maxPocetOchrankaru += okolik;
	}

	public double getZisk() // mozno upravit + zkontrolovat hodnotu pom + zkontrolovat return pro koupovane
	{

		double pom = 0;
		for (int i = 0; i < ochranka.size(); i++)
		{
			pom += ochranka.get(i).getPlat();
		}

		if(okupovane == false)
		{
			return level * zisk - pom;
		}
		else
		{
			return level * zisk - pom - vypalne;
		}
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setZisk(double okolik)
	{
		zisk += okolik; // upravit
	}

}