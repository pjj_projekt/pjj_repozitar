package cz.mendelu.pjj.projekt;

public class Hrac
{

	private String jmeno;
	private int respekt;
	private double penize;
	private Zakladna zakladna;
	private int pocetAkciZaKolo;
	private int zbyvajiciAkce;

	/**
	 * 
	 * @param jmeno
	 */
	public Hrac(String jmeno)
	{
		this.jmeno = jmeno;
		this.respekt = 10; // upravit
		Zakladna zakladna = new Zakladna();
		this.pocetAkciZaKolo = 5; // upravit
		this.zbyvajiciAkce = pocetAkciZaKolo;
		this.penize = 2000; // upravit

	}

	public int getRespekt()
	{
		return this.respekt;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setRespekt(int okolik)
	{
		this.respekt += okolik;
	}

	public double getPenize()
	{
		return this.penize;
	}

	/**
	 * 
	 * @param budova
	 */
	public void interagujSbudovou(Building budova) // asi to radeji udelat v Building -->> public void interagujShracem(Hrac hrac);
	{
		// tady pridat asi nejaky command
		// TODO - implement Hrac.interagujSbudovou
		throw new UnsupportedOperationException();
	}

	public int getPocetAkciZaKolo()
	{
		return this.pocetAkciZaKolo;
	}

	/**
	 * 
	 * @param kolik
	 */
	public void setPocetAkciZaKolo(int kolik) // mozna okolik
	{
		this.pocetAkciZaKolo = kolik;
	}

	public int getZbyvajiciAkce()
	{
		return this.zbyvajiciAkce;
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setZbyvajiciAkce(int oKolik)// zkontrolovat znaminka -> jestli +=okolik nebo -=okolik
	{
		zbyvajiciAkce -= oKolik;
	}


	public void setPenize(double penize)
	{
		this.penize = penize;
	}

	public  Zakladna getZakladna()
	{
		return this.zakladna;

	}

}