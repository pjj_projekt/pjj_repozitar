package cz.mendelu.pjj.projekt;

public class Poskok extends Clovek
{

	private int loajalita;

	/**
	 * 
	 * @param loajalita
	 * @param zivoty
	 * @param zbran
	 * @param presnost
	 * @param kryti
	 */
	public Poskok(int loajalita, int zivoty, Zbran zbran, Zbroj zbroj, int presnost, int kryti)
	{
		super(zivoty, zbran, zbroj, presnost, kryti);
		this.loajalita = loajalita;
	}

	public int getLoajalita()
	{
		return this.loajalita;
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setLoajalita(int oKolik)
	{
		this.loajalita += oKolik;
	}

	public void printInfo()
	{
		System.out.println("loajalita: " + loajalita);
		System.out.println("zivoty: " + zivoty);
		System.out.println("zbran: " ); zbran.printInfo();
		System.out.println("zbroj: "); zbroj.printInfo();

	}

}