package cz.mendelu.pjj.projekt;

import java.util.LinkedList;

public class Sklad
{

	private int level;
	private LinkedList<Surovina> zasoby;
	private int maxKapacita;
	private int volneMisto;
	private LinkedList<Surovina> pomZasoby; //tohle mozna odstranit

	public Sklad()
	{
		maxKapacita = 50; // upravit
		this.zasoby = new LinkedList();
		this.zasoby.clear();
		calculateVolneMisto();
		this.pomZasoby.clear(); // tohle mozna odstranit
	}

	public Surovina getSurovinaAt(int i)
	{
		return zasoby.get(i);
	}

	/**
	 * 
	 * @param surovina
	 */
	public boolean ulozSurovinu(Surovina surovina)
	{
		if(surovina.velikostPolozky <= volneMisto)
		{
			zasoby.addLast(surovina);
			calculateVolneMisto();
			return true;
		}
		else
		{
			System.out.println("Surovinu neni mozne ulozit ve skladisti, protoze je nedostatek mista");
			return false;
		}
	}

	/**
	 * 
	 * @param surovina
	 */
	public boolean zahodSurovinu(Surovina surovina)
	{
		// tu se asi bude resit jenom odstraneni suroviny a volne misto
		//double pomNaPenize = surovina.getCena(); // zkontrolovat, jestli to pracuje jak ma -> ulozi se do promennej spravna cena spravne suroviny ===>>> tohle se bude pravdepodobne resit v Market prodejSurovinu()

		// pridat if, ktery bude spojeny s getSuroviny()
		// zavola se metoda getSuroviny() --> ze seznamu se vybere ktera surovina "se proda/ resp. zahodi"
		// mozna bude potreba jeste pridat metodu, z ktere se vybere surovina, ktera se zahodi => neco jako metoda(Surovina surovina){getSuroviny(surovina); return konktretni surovina;}
		{
			zasoby.remove(surovina);
			calculateVolneMisto();
			return true;
		}
		// pridat hracovi penize -> hrac.setMoney(pomNaPenize); ==>> tohle se pravdepodobne bude resit v Market prodejSurovinu
	}

	/**
	 * 
	 * @param okolik
	 */
	public void zvetsiKapacitu(int okolik)
	{
		this.maxKapacita += okolik;
	}

	public int getMaxKapacita()
	{
		return this.maxKapacita;
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setMaxKapacita(int oKolik)
	{
		this.maxKapacita = oKolik;
	}

	public int getVolneMisto()
	{
		return this.volneMisto;
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setVolneMisto(int oKolik)
	{
		this.volneMisto = oKolik;
	}

	public void calculateVolneMisto()
	{
		this.volneMisto = maxKapacita - zasoby.size();
	}



	public LinkedList getSuroviny(Surovina surovina) // tohle je potreba otestovat => melo by to vratit seznam surovin daneho typu; mozna jako parametr bude String
	{
		LinkedList<Surovina> pomSuroviny = new LinkedList<>();
		pomSuroviny.clear();

		for(int i = 0; i < zasoby.size(); i++)
		{
			if(zasoby.get(i).getTypSuroviny() == surovina.getTypSuroviny())
			{
				pomSuroviny.addLast(zasoby.get(i));
			}
		}

		//return zasoby.get(surovina); -> spis return seznamSurovin
		//zatim -> vvvvvvv
		//return null;
		pomZasoby = pomSuroviny; // mozna tohle odstranit
		return pomSuroviny;
	}

	public void seznamZasob()	// zasoby, ktere se nachazeji v sklade
	{
		for(int i = 0; i < zasoby.size(); i++)
		{
		 	System.out.print(zasoby.get(i).getTypSuroviny());
		}
	}


}