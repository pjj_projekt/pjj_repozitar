package cz.mendelu.pjj.projekt;

public abstract class Surovina
{

	protected double cena;
	protected int velikostPolozky;
	protected String typSuroviny;

	/**
	 * 
	 * @param cena
	 * @param velikostPolozky
	 */
	public Surovina(double cena, int velikostPolozky)
	{
		this.cena = cena;
		this.velikostPolozky = velikostPolozky;
	}

	public abstract double getCena();

	/**
	 * 
	 * @param okolik
	 */
	public abstract void setCena(double okolik);

	public int getVelikostPolozky()
	{
		return velikostPolozky;
	}

	public String getTypSuroviny()
	{
		return this.typSuroviny;
	}
}