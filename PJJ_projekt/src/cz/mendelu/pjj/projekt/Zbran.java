package cz.mendelu.pjj.projekt;

public class Zbran
{

	private int utok;
	private int presnostZbrane;

	/**
	 * 
	 * @param utok
	 * @param presnostZbrane
	 */
	public Zbran(int utok, int presnostZbrane)
	{
		this.utok = utok;
		this.presnostZbrane = presnostZbrane;
	}

	public int getUtok()
	{
		return this.utok;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setUtok(int okolik)
	{
		this.utok = okolik;
	}

	public int getPresnostZbrane()
	{
		return this.presnostZbrane;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setPresnostZbrane(int okolik)
	{
		this.presnostZbrane = okolik;
	}

	public void upgradeZbrane()
	{
		// TODO - implement Zbran.upgradeZbrane
		throw new UnsupportedOperationException();
	}

	public void printInfo()
	{
		System.out.println("utok: " + utok);
		System.out.println("presnost: " + presnostZbrane);
	}


}