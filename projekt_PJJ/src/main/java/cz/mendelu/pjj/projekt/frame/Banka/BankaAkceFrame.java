package cz.mendelu.pjj.projekt.frame.Banka;

import cz.mendelu.pjj.projekt.frame.ValueEnterFrame;
import cz.mendelu.pjj.projekt.model.*;

import javax.swing.*;

public class BankaAkceFrame extends JFrame
{
    private JButton button_koupitBudovu;
    private JButton button_prodatBudovu;
    private JButton button_vylepsitBudovu;
    private JButton button_vzitSiPujcku;
    private JButton button_najmoutOchranku;
    private JPanel panel_bankaAkce;
    private JButton button_back;

    private ValueEnterFrame valueEnterFrame;



    // pomocne promenne pro ochranku

      double plat = 100;
      int zivoty = 30;
      Zbran zbran = new Zbran(5,6);
      Zbroj zbroj = new Zbroj(7);
      int  presnot = 8;
      int  kryti = 9;


    private Hrac hrac;
    private Banka banka;


    // private Banka banka;
    //private Mesto mesto = Mesto.getMesto();

    //public BankaAkceFrame(City city)
    public BankaAkceFrame(City city, Hrac hrac)
    {
        banka = city.getBanka();
        //this.hrac = city.getHrac();
        this.hrac = hrac;


        button_koupitBudovu.addActionListener(e -> {
            hrac.koupitBudovu(banka);
            this.dispose();
        });
        button_prodatBudovu.addActionListener(e -> {
            hrac.prodatBudovu(banka);
            this.dispose();
        });
        button_vylepsitBudovu.addActionListener(e ->
        {
            banka.setLevel(1);
            banka.getVlastnik().setPenize(banka.getVlastnik().getPenize() - banka.getCenaZaLevel());
            this.dispose();
        });

        button_vzitSiPujcku.addActionListener(e ->
        {
            valueEnterFrame = new ValueEnterFrame(banka, hrac);
            valueEnterFrame.setVisible(true);
            valueEnterFrame.setContentPane(valueEnterFrame.getPanel_pujcka());
            valueEnterFrame.pack();
            valueEnterFrame.setSize(650, 350);


            //double pomPujcka = valueEnterFrame.getHodnota();
            //System.out.println(pomPujcka); // kontrola -> 0 WHY??

            //banka.vzitSiPujcku(mesto.getHrac1(), pomPujcka);
            //banka.vzitSiPujcku(hrac, pomPujcka);
            //hrac.vzitSiPujcku(banka, pomPujcka);
        });

        button_najmoutOchranku.addActionListener(e -> {
            banka.najmoutOchranku(new Ochranka(plat, zivoty, zbran, zbroj, presnot, kryti));
            this.dispose();
        });

        button_back.addActionListener(e -> this.dispose());

    }


    public JPanel getPanel_bankaAkce()
    {
        return panel_bankaAkce;
    }
}


