package cz.mendelu.pjj.projekt.frame.Banka;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BankaFrame
{
    private JPanel panel_banka;
    private JButton button_info;
    private JButton button_akce;
    private JButton button_odejit;


    private JFrame frame;


    //public BankaFrame(JFrame jFrame, City city)
    public BankaFrame(JFrame frame, City city, Hrac hrac)
    {
        this.frame = frame;



        button_info.addActionListener(new ActionListener()
        {

            BankaInfoFrame jFramebankaInfo;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFramebankaInfo = new BankaInfoFrame(city, hrac);
                jFramebankaInfo.setVisible(true);
                jFramebankaInfo.setContentPane(jFramebankaInfo.getPanel_barInfo());
                jFramebankaInfo.pack();
                jFramebankaInfo.setSize(650, 350);
            }
        });


        button_akce.addActionListener(new ActionListener()
        {
            BankaAkceFrame jFrameBankaAkce;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrameBankaAkce = new BankaAkceFrame(city, hrac);
                jFrameBankaAkce.setVisible(true);
                jFrameBankaAkce.setContentPane(jFrameBankaAkce.getPanel_bankaAkce());
                jFrameBankaAkce.pack();
                jFrameBankaAkce.setSize(650, 350);

            }
        });


        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                frame.dispose();
            }
        });


    }

    public JPanel getPanel_banka()
    {
        return panel_banka;
    }
}
