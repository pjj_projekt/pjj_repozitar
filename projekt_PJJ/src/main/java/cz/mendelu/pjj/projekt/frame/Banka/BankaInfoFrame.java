package cz.mendelu.pjj.projekt.frame.Banka;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;

public class BankaInfoFrame extends JFrame
{
    private JTextField textField_nazev;
    private JTextField textField_level;
    private JTextField textField_zisk;
    private JTextField textField_vlastnik;
    private JTextField textField_pocetOcrankaru;
    private JTextField textField_pocetPoskoku;
    private JTextField textField_maxPocetOchrankaru;
    private JPanel panel_bankaInfo;
    private JButton button_back;


    //public BankaInfoFrame(City city)
    public BankaInfoFrame(City city, Hrac hrac)
    {
        textField_nazev.setText((city.getBanka().getNazev()));
        textField_level.setText(String.valueOf(city.getBanka().getLevel()));
        textField_zisk.setText(String.valueOf(city.getBanka().getZisk()));
        textField_pocetOcrankaru.setText(String.valueOf(city.getBanka().getPocetOchrankaru()));
        textField_maxPocetOchrankaru.setText(String.valueOf(city.getBanka().getMaxPocetOchrankaru()));
        textField_pocetPoskoku.setText(String.valueOf(city.getBanka().getMaxPocetLidi()));


        button_back.addActionListener(e -> this.dispose());

        try
        {
            if (city.getBanka().getVlastnik().getJmeno() != null)
            {
                textField_vlastnik.setText(city.getBanka().getVlastnik().getJmeno());
            }
        }
        catch (Exception e)
        {
            textField_vlastnik.setText("---");
        }



    }

    public JPanel getPanel_barInfo()
    {
        return panel_bankaInfo;
    }
}
