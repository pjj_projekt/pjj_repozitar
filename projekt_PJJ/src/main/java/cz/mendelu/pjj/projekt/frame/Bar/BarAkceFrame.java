package cz.mendelu.pjj.projekt.frame.Bar;

import cz.mendelu.pjj.projekt.model.Bar;
import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;




public class BarAkceFrame extends JFrame
{
    private JButton button_koupitBudovu;
    private JButton button_prodatBudovu;
    private JButton button_vylepsitBudovu;
    private JPanel panel_BarAkce;
    private JButton button_back;


    private Hrac hrac;

    public JPanel getPanel_BarAkce()
    {
        return panel_BarAkce;
    }

    private Bar bar;


    public BarAkceFrame(City city, Hrac hrac)
    {
        bar = city.getBar();
        //this.hrac = city.getHrac();
        this.hrac = hrac;


        button_koupitBudovu.addActionListener(e -> {
            hrac.koupitBudovu(bar);
            this.dispose();
        });
        button_prodatBudovu.addActionListener(e -> {
            hrac.prodatBudovu(bar);
            this.dispose();
        });
        button_vylepsitBudovu.addActionListener(e ->
        {
            bar.setLevel(1);
            bar.getVlastnik().setPenize(bar.getVlastnik().getPenize() - bar.getCenaZaLevel());
            this.dispose();
        });

        button_back.addActionListener(e -> this.dispose());
    }
}
