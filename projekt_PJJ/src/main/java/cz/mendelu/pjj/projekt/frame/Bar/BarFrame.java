package cz.mendelu.pjj.projekt.frame.Bar;


import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BarFrame
{
    private JButton button_info;
    private JButton button_akce;
    private JButton button_odejit;
    private JPanel panel_bar;


    private JFrame jFrame;

    public BarFrame(JFrame jFrame, City city, Hrac hrac)
    {

        this.jFrame = jFrame;


        button_info.addActionListener(new ActionListener()
        {
            BarInfoFrame jFramebarInfo;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFramebarInfo = new BarInfoFrame(city, hrac);
                jFramebarInfo.setVisible(true);
                jFramebarInfo.setContentPane(jFramebarInfo.getPanel_barInfo());
                jFramebarInfo.pack();
                jFramebarInfo.setSize(650, 350);

            }
        });


        button_akce.addActionListener(new ActionListener()
        {
            BarAkceFrame jFrameBarAkce;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrameBarAkce = new BarAkceFrame(city, hrac);
                jFrameBarAkce.setVisible(true);
                jFrameBarAkce.setContentPane(jFrameBarAkce.getPanel_BarAkce());
                jFrameBarAkce.pack();
                jFrameBarAkce.setSize(650, 350);
            }
        });


        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrame.dispose();
            }
        });
    }


    public JPanel getPanel_bar()
    {
        return panel_bar;
    }
}
