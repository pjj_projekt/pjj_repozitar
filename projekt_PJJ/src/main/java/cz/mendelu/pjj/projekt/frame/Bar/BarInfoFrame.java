package cz.mendelu.pjj.projekt.frame.Bar;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;
import java.awt.*;

public class BarInfoFrame extends JFrame
{
    private JTextField textField_nazev;
    private JTextField textField_level;
    private JTextField textField_zisk;
    private JTextField textField_vlastnik;
    private JTextField textField_pocetStamgastu;
    private JTextField textField_maxPocetStamgastu;
    private JTextField textField_pocetPoskoku;
    private JPanel panel_barInfo;
    private JButton button_back;


    public BarInfoFrame(City city, Hrac hrac)
    {
        textField_nazev.setText((city.getBanka().getNazev()));
        textField_level.setText(String.valueOf(city.getBanka().getLevel()));
        textField_zisk.setText(String.valueOf(city.getBanka().getZisk()));
        textField_pocetStamgastu.setText(String.valueOf(city.getBanka().getPocetOchrankaru()));
        textField_maxPocetStamgastu.setText(String.valueOf(city.getBanka().getMaxPocetOchrankaru()));
        textField_pocetPoskoku.setText(String.valueOf(city.getBanka().getMaxPocetLidi()));


        button_back.addActionListener(e -> this.dispose());

        try
        {
            if (city.getBar().getVlastnik().getJmeno() != null)
            {
                textField_vlastnik.setText(city.getBar().getVlastnik().getJmeno());
            }
        }
        catch (Exception e)
        {
            textField_vlastnik.setText("---");
        }

    }

    public Container getPanel_barInfo()
    {
        return panel_barInfo;
    }
}
