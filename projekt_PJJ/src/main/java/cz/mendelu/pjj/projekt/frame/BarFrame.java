package cz.mendelu.pjj.projekt.frame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BarFrame
{
    private JButton button_info;
    private JButton button_akce;
    private JButton button_odejit;
    private JPanel panel_bar;


    private JFrame jFrame;

    public BarFrame(JFrame jFrame)
    {

        this.jFrame = jFrame;


        button_info.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });


        button_akce.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });


        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrame.dispose();
            }
        });
    }


    public JPanel getPanel_bar()
    {
        return panel_bar;
    }
}
