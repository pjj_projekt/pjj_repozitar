package cz.mendelu.pjj.projekt.frame.Hotel;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hotel;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;

public class HotelAkceFrame extends JFrame
{
    private JButton button_koupitBudovu;
    private JButton button_prodatBudovu;
    private JButton button_vylepsitBudovu;
    private JPanel panel_hotelAkce;
    private JButton button_back;

    private Hrac hrac;
    private Hotel hotel;

    public HotelAkceFrame(City city, Hrac hrac)
    {
        hotel = city.getHotel();
        //this.hrac = city.getHrac();
        this.hrac = hrac;


        button_koupitBudovu.addActionListener(e -> {
            hrac.koupitBudovu(hotel);
            this.dispose();
        });
        button_prodatBudovu.addActionListener(e -> {
            hrac.prodatBudovu(hotel);
            this.dispose();
        });
        button_vylepsitBudovu.addActionListener(e ->
        {
            hotel.setLevel(1);
            hotel.getVlastnik().setPenize(hotel.getVlastnik().getPenize() - hotel.getCenaZaLevel());
            this.dispose();
        });

        button_back.addActionListener(e -> this.dispose());

    }


    public JPanel getPanel_hotelAkce()
    {
        return panel_hotelAkce;
    }
}
