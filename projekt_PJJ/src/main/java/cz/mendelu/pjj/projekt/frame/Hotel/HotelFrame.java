package cz.mendelu.pjj.projekt.frame.Hotel;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HotelFrame
{
    private JButton button_info;
    private JButton button_akce;
    private JButton button_odejit;
    private JPanel panel_hotel;

    private JFrame jframe;


    public HotelFrame(JFrame jframe, City city, Hrac hrac)
    {
        this.jframe = jframe;


        button_info.addActionListener(new ActionListener()
        {
            HotelInfoFrame jFrameHotelInfo;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrameHotelInfo = new HotelInfoFrame(city, hrac);
                jFrameHotelInfo.setVisible(true);
                jFrameHotelInfo.setContentPane(jFrameHotelInfo.getPanel_hotelInfo());
                jFrameHotelInfo.pack();
                jFrameHotelInfo.setSize(650, 350);
            }
        });
        button_akce.addActionListener(new ActionListener()
        {

            HotelAkceFrame jFrameHotelAkce;


            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrameHotelAkce = new HotelAkceFrame(city, hrac);
                jFrameHotelAkce.setVisible(true);
                jFrameHotelAkce.setContentPane(jFrameHotelAkce.getPanel_hotelAkce());
                jFrameHotelAkce.pack();
                jFrameHotelAkce.setSize(650, 350);

            }
        });
        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                jframe.dispose();
            }
        });
    }

    public JPanel getPanel_hotel()
    {
        return panel_hotel;
    }
}
