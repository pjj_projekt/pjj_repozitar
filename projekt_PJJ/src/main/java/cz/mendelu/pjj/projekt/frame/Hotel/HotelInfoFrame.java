package cz.mendelu.pjj.projekt.frame.Hotel;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;

public class HotelInfoFrame extends JFrame
{
    private JTextField textField_nazev;
    private JTextField textField_level;
    private JTextField textField_zisk;
    private JTextField textField_vlastnik;
    private JTextField textField_pocetApartmanu;
    private JTextField textField_pocetPoskoku;
    private JPanel panel_hotelInfo;
    private JButton button_back;


    public HotelInfoFrame(City city, Hrac hrac)
    {

        textField_nazev.setText((city.getHotel()).getNazev());
        textField_level.setText(String.valueOf(city.getHotel().getLevel()));
        textField_zisk.setText(String.valueOf(city.getHotel().getZisk()));
        textField_pocetApartmanu.setText(String.valueOf(city.getHotel().getPocetApartmanu()));
        textField_pocetPoskoku.setText(String.valueOf(city.getHotel().getMaxPocetLidi()));

        button_back.addActionListener(e -> this.dispose());


        try
        {
            if (city.getHotel().getVlastnik().getJmeno() != null)
            {
                textField_vlastnik.setText(city.getHotel().getVlastnik().getJmeno());
            }
        }
        catch (Exception e)
        {
            textField_vlastnik.setText("---");
        }

    }

    public JPanel getPanel_hotelInfo()
    {
        return panel_hotelInfo;
    }
}
