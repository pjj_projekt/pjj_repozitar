package cz.mendelu.pjj.projekt.frame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HotelFrame
{
    private JButton button_info;
    private JButton button_akce;
    private JButton button_odejit;
    private JPanel panel_hotel;

    private JFrame jframe;


    public HotelFrame(JFrame jframe)
    {
        this.jframe = jframe;


        button_info.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });
        button_akce.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });
        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                jframe.dispose();
            }
        });
    }

    public JPanel getPanel_hotel()
    {
        return panel_hotel;
    }
}
