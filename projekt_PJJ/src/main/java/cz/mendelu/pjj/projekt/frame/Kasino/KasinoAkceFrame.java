package cz.mendelu.pjj.projekt.frame.Kasino;

import cz.mendelu.pjj.projekt.model.Banka;
import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import cz.mendelu.pjj.projekt.model.Kasino;

import javax.swing.*;

public class KasinoAkceFrame extends JFrame
{
    private JButton button_koupitBudovu;
    private JButton button_prodatBudovu;
    private JButton button_vylepsitBudovu;
    private JPanel panel_kasinoAkce;
    private JButton button_back;

    private Hrac hrac;
    private Kasino kasino;




    public KasinoAkceFrame(City city, Hrac hrac)
    {

        kasino = city.getKasino();
        //this.hrac = city.getHrac();
        this.hrac = hrac;


        button_koupitBudovu.addActionListener(e -> {
            hrac.koupitBudovu(kasino);
            this.dispose();
        });
        button_prodatBudovu.addActionListener(e -> {
            hrac.prodatBudovu(kasino);
            this.dispose();
        });
        button_vylepsitBudovu.addActionListener(e ->
        {
            kasino.setLevel(1);
            kasino.getVlastnik().setPenize(kasino.getVlastnik().getPenize() - kasino.getCenaZaLevel());
            this.dispose();
        });

        button_back.addActionListener(e -> this.dispose());

    }

    public JPanel getPanel_kasinoAkce()
    {
        return panel_kasinoAkce;
    }
}
