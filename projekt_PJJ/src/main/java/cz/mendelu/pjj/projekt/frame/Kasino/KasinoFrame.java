package cz.mendelu.pjj.projekt.frame.Kasino;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import cz.mendelu.pjj.projekt.model.Kasino;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class KasinoFrame
{
    private JButton button_info;
    private JButton button_akce;
    private JButton button_odejit;
    private JPanel panel_kasino;

    private JFrame jFrame;

    public KasinoFrame(JFrame jFrame, City city, Hrac hrac)
    {

        this.jFrame = jFrame;


        button_info.addActionListener(new ActionListener()
        {
            KasinoInfoFrame jFrameKasinoaInfo;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrameKasinoaInfo = new KasinoInfoFrame(city, hrac);
                jFrameKasinoaInfo.setVisible(true);
                jFrameKasinoaInfo.setContentPane(jFrameKasinoaInfo.getPanel_kasinoInfo());
                jFrameKasinoaInfo.pack();
                jFrameKasinoaInfo.setSize(650, 350);
            }
        });


        button_akce.addActionListener(new ActionListener()
        {
            KasinoAkceFrame jFrameKasinoAkce;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrameKasinoAkce = new KasinoAkceFrame(city, hrac);
                jFrameKasinoAkce.setVisible(true);
                jFrameKasinoAkce.setContentPane(jFrameKasinoAkce.getPanel_kasinoAkce());
                jFrameKasinoAkce.pack();
                jFrameKasinoAkce.setSize(650, 350);
            }
        });


        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrame.dispose();
            }
        });


    }


    public JPanel getPanel_kasino()
    {
        return panel_kasino;
    }
}
