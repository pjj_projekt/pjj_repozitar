package cz.mendelu.pjj.projekt.frame.Kasino;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;

public class KasinoInfoFrame extends JFrame
{
    private JPanel panel_kasinoInfo;
    private JTextField textField_nazev;
    private JTextField textField_level;
    private JTextField textField_zisk;
    private JTextField textField_vlastnik;
    private JTextField textField_vybavenost;
    private JTextField textField_pocetPoskoku;
    private JButton button_back;


    public KasinoInfoFrame(City city, Hrac hrac)
    {

        textField_nazev.setText((city.getKasino().getNazev()));
        textField_level.setText(String.valueOf(city.getKasino().getLevel()));
        textField_zisk.setText(String.valueOf(city.getKasino().getZisk()));
        textField_vybavenost.setText(String.valueOf(city.getKasino().getVybavenost()));
        textField_pocetPoskoku.setText(String.valueOf(city.getKasino().getMaxPocetLidi()));

        button_back.addActionListener(e -> this.dispose());

        try
        {
            if (city.getKasino().getVlastnik().getJmeno() != null)
            {
                textField_vlastnik.setText(city.getKasino().getVlastnik().getJmeno());
            }
        }
        catch (Exception e)
        {
            textField_vlastnik.setText("---");
        }


    }



    public JPanel getPanel_kasinoInfo()
    {
        return panel_kasinoInfo;
    }
}
