package cz.mendelu.pjj.projekt.frame.Market;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MarketFrame
{
    private JButton button_info;
    private JButton button_akce;
    private JButton button_odejit;
    private JPanel panel_market;

    private JFrame jFrame;


    public MarketFrame(JFrame jFrame)
    {
        this.jFrame = jFrame;


        button_info.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });


        button_akce.addActionListener(new ActionListener()
        {
            MarketAkceFrame jFrameMarketAkce;

            @Override
            public void actionPerformed(ActionEvent e)
            {
                this.jFrameMarketAkce = new MarketAkceFrame();
                this.jFrameMarketAkce.setVisible(true);
                this.jFrameMarketAkce.setContentPane(jFrameMarketAkce.getPanel_MarketAkce());
                this.jFrameMarketAkce.pack();
                this.jFrameMarketAkce.setSize(650, 350);
            }
        });


        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                jFrame.dispose();
            }
        });


    }


    public JPanel getPanel_market()
    {
        return panel_market;
    }
}
