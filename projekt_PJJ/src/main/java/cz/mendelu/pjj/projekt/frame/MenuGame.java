package cz.mendelu.pjj.projekt.frame;

import cz.mendelu.pjj.projekt.io.JsonContainer;
import cz.mendelu.pjj.projekt.io.JsonLibrary;
import cz.mendelu.pjj.projekt.gf.MyWorld;
import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import greenfoot.Greenfoot;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;

public class MenuGame
{
    private JPanel panel_MenuGame;
    private JButton button_return;
    private JButton button_save;
    private JButton button_load;
    private JButton button_quit;
    private JButton button_newGame;

    private JFrame jFrame;

    private City city;
    private Hrac hrac;





    private static final String FILE = "GAME_SAVE.bin";


    public MenuGame(City city, Hrac hrac)
    {
        this.city = city;
        this.hrac = hrac;


        this.jFrame = new JFrame("MenuGame");
        this.jFrame.setVisible(true);
        this.jFrame.setContentPane(panel_MenuGame);
        this.jFrame.pack();

        this.jFrame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowOpened(WindowEvent e)
            {
               hideMenu(false);
            }
        });


        //RETURN
        button_return.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                    Greenfoot.start();
                    hideMenu(true); // po kliku na tlacitko se schova menu

            }
        });


        //SAVE
        button_save.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

                try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILE)))
                {
                    oos.writeObject(city);
                    //oos.writeObject(mesto);
                    oos.writeObject(hrac);


                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                finally
                {
                    //Greenfoot.start();
                    hideMenu(true); // po kliku na tlacitko se schova menu
                }

            }
        });


        //LOAD
        button_load.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE)))
                {
                    City city = (City) ois.readObject();
                    Hrac hrac = (Hrac) ois.readObject();
                    MyWorld newWorld = new MyWorld(city, hrac);
                    Greenfoot.setWorld(newWorld);



                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }

                finally {
                    Greenfoot.start();
                    hideMenu(true); // po kliku na tlacitko se schova menu
                }

           }
        });


        //QUIT
        button_quit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                System.exit(0);
            }
        });


        //NEW GAME
        button_newGame.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Greenfoot.setWorld(new MyWorld());
                hideMenu(true);
            }
        });

    }



    public JPanel getPanel_menuGame()
    {
        return panel_MenuGame;
    }




    public void openMenu()
    {
        Greenfoot.stop();
        SwingUtilities.invokeLater(() -> {
            jFrame.setVisible(true);
        });
    }


    public void hideMenu(boolean dispose)
    {
        Greenfoot.start();
        SwingUtilities.invokeLater(() ->
        {
            jFrame.setVisible(false);
            if (dispose)
            {
                jFrame.dispose();
            }
        });
    }

}
