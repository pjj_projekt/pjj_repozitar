package cz.mendelu.pjj.projekt.frame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewGameFrame
{
    private JButton button_singleplayer;
    private JButton button_multiplayer;
    private JButton button_back;
    private JPanel panel_newGame;

    public NewGameFrame()
    {
        button_singleplayer.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });

        button_multiplayer.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });

        button_back.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                //new UvodFrame().setVisible(true);
                //this.setVisible(false);
            }
        });
    }

    public JPanel getPanel_newGame()
    {
        return panel_newGame;
    }
}
