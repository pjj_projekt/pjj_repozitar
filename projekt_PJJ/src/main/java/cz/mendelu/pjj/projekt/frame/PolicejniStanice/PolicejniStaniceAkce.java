package cz.mendelu.pjj.projekt.frame.PolicejniStanice;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import cz.mendelu.pjj.projekt.model.PolicejniStanice;

import javax.swing.*;

public class PolicejniStaniceAkce extends JFrame
{


    private JButton button_uplatit;
    private JButton button_back;
    private JPanel panel_policejniStaniceAkce;

    private PolicejniStaniceUplatitFrame policejniStaniceUplatitFrame;


    private Hrac hrac;
    private PolicejniStanice policejniStanice;

    public PolicejniStaniceAkce(City city, Hrac hrac)
    {
        this.hrac = hrac;
        this.policejniStanice = city.getPolicejniStanice();


        button_uplatit.addActionListener(e -> {
            policejniStaniceUplatitFrame = new PolicejniStaniceUplatitFrame(policejniStanice, hrac);
            policejniStaniceUplatitFrame.setVisible(true);
            policejniStaniceUplatitFrame.setContentPane(policejniStaniceUplatitFrame.getPanel_uplatit());
            policejniStaniceUplatitFrame.pack();
            policejniStaniceUplatitFrame.setSize(650, 350);
        });

        button_back.addActionListener(e -> this.dispose());
    }


    public JPanel getPanel_policejniStaniceAkce()
    {
        return panel_policejniStaniceAkce;
    }

    private void createUIComponents()
    {
        // TODO: place custom component creation code here
    }
}
