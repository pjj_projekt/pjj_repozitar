package cz.mendelu.pjj.projekt.frame.PolicejniStanice;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PolicejniStaniceFrame
{
    private JPanel panel_policejniStanice;
    private JButton button_akce;
    private JButton button_odejit;
    private JButton button_info;



    private JFrame frame;



    public PolicejniStaniceFrame(JFrame frame, City city, Hrac hrac)
    {
        this.frame = frame;


        button_akce.addActionListener(new ActionListener()
        {
            PolicejniStaniceAkce jFramePolicejniStaniceAkce;


            @Override
            public void actionPerformed(ActionEvent e)
            {

                jFramePolicejniStaniceAkce = new PolicejniStaniceAkce(city, hrac);
                jFramePolicejniStaniceAkce.setVisible(true);
                jFramePolicejniStaniceAkce.setContentPane(jFramePolicejniStaniceAkce.getPanel_policejniStaniceAkce());
                jFramePolicejniStaniceAkce.pack();
                jFramePolicejniStaniceAkce.setSize(650, 350);


            }
        });


        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                frame.dispose();
            }
        });


        button_info.addActionListener(new ActionListener()
        {
            PolicejniStaniceInfo jFramePolicejniStaniceInfo;


            @Override
            public void actionPerformed(ActionEvent e)
            {

                jFramePolicejniStaniceInfo = new PolicejniStaniceInfo(city, hrac);
                jFramePolicejniStaniceInfo.setVisible(true);
                jFramePolicejniStaniceInfo.setContentPane(jFramePolicejniStaniceInfo.getPanel_policejniStaniceInfo());
                jFramePolicejniStaniceInfo.pack();
                jFramePolicejniStaniceInfo.setSize(650, 350);
            }
        });
    }

    public JPanel getPanel_policejniStanice()
    {
        return panel_policejniStanice;
    }
}
