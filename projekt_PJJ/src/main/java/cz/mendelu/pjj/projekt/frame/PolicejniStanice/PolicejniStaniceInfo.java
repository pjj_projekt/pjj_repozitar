package cz.mendelu.pjj.projekt.frame.PolicejniStanice;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;

public class PolicejniStaniceInfo extends JFrame
{
    private JTextField textField_nazev;
    private JPanel panel_policejniStaniceInfo;
    private JButton button_back;

    public PolicejniStaniceInfo(City city, Hrac hrac)
    {
        textField_nazev.setText("...........POLICIE...........");

        button_back.addActionListener(e -> this.dispose());

    }


    public JPanel getPanel_policejniStaniceInfo()
    {
        return panel_policejniStaniceInfo;
    }
}
