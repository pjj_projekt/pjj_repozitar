package cz.mendelu.pjj.projekt.frame.PolicejniStanice;

import cz.mendelu.pjj.projekt.model.Hrac;
import cz.mendelu.pjj.projekt.model.PolicejniStanice;

import javax.swing.*;

public class PolicejniStaniceUplatitFrame extends JFrame
{
    private JPanel panel_uplatit;
    private JTextField textField1;
    private JButton potvrditButton;
    private JButton zrusitButton;


    private  double hodnota;


    public PolicejniStaniceUplatitFrame(PolicejniStanice policejniStanice, Hrac hrac)
    {
        potvrditButton.addActionListener(e ->
        {
            this.hodnota = Double.valueOf(textField1.getText());
            policejniStanice.uplatit(hodnota, hrac);


            this.dispose();
        });
        zrusitButton.addActionListener(e -> this.dispose());

    }

    public JPanel getPanel_uplatit()
    {
        return panel_uplatit;
    }
}
