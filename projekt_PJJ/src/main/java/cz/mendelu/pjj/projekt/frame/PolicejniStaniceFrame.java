package cz.mendelu.pjj.projekt.frame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PolicejniStaniceFrame
{
    private JPanel panel_policejniStanice;
    private JButton button_akce;
    private JButton button_odejit;
    private JButton button_info;



    private JFrame frame;



    public PolicejniStaniceFrame(JFrame frame)
    {
        this.frame = frame;


        button_akce.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });


        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                frame.dispose();
            }
        });


        button_info.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });
    }

    public JPanel getPanel_policejniStanice()
    {
        return panel_policejniStanice;
    }
}
