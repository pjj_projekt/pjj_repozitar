package cz.mendelu.pjj.projekt.frame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UvodFrame
{
    private JPanel panel_uvod;
    private JButton button_newGame;
    private JButton button_loadGame;
    private JButton button_credits;
    private JButton button_quitGame;


    public JPanel getPanel_uvod()
    {
        return panel_uvod;
    }

    public UvodFrame()
    {

        button_newGame.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // zacat novu hru
                //new NewGameFrame().setVisible(true);
                //this.setVisible(false);
            }
        });

        button_loadGame.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // nacitat hru
            }
        });

        button_credits.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // vypisat info
            }
        });

        button_quitGame.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // ukoncit hru
                System.exit(0);
            }
        });
    }


}
