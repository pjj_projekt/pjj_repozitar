package cz.mendelu.pjj.projekt.frame;

import cz.mendelu.pjj.projekt.model.Banka;
import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;

public class ValueEnterFrame extends JFrame
{
    private JTextField textField1;
    private JButton potvrditButton;
    private JButton zrusitButton;
    private JPanel panel_pujcka;



    private double hodnota;



    public ValueEnterFrame(Banka banka, Hrac hrac)
    {

        potvrditButton.addActionListener(e ->
        {
            this.hodnota = Double.valueOf(textField1.getText());
            banka.vzitSiPujcku(hrac, hodnota);

            System.out.println(this.hodnota+"valeueenterframe"); // kontrola

            this.dispose();
        });
        zrusitButton.addActionListener(e -> this.dispose());


    }

    public JPanel getPanel_pujcka()
    {
        return panel_pujcka;
    }




}
