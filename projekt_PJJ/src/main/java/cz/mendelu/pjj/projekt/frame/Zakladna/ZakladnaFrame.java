package cz.mendelu.pjj.projekt.frame.Zakladna;

import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ZakladnaFrame
{
    private JPanel panel_zakladna;
    private JButton button_info;
    private JButton button_akce;
    private JButton button_odejit;


    private JFrame jframe;



    public ZakladnaFrame(JFrame jframe, Hrac hrac)

    {
        this.jframe = jframe;

        button_info.addActionListener(new ActionListener()
        {
            ZakladnaInfoFrame jFrameZakladnaInfo;


            @Override
            public void actionPerformed(ActionEvent e)
            {

                jFrameZakladnaInfo = new ZakladnaInfoFrame(hrac);
                jFrameZakladnaInfo.setVisible(true);
                jFrameZakladnaInfo.setContentPane(jFrameZakladnaInfo.getPanel_zakladnaInfo());
                jFrameZakladnaInfo.pack();
                //jFrameZakladnaInfo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                jFrameZakladnaInfo.setSize(650, 350);


            }
        });
        button_akce.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });
        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                jframe.dispose();
            }
        });
    }

    public JPanel getPanel_zakladna()
    {
        return panel_zakladna;
    }
}
