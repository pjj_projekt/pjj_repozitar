package cz.mendelu.pjj.projekt.frame.Zakladna;


import cz.mendelu.pjj.projekt.model.Hrac;
import cz.mendelu.pjj.projekt.model.Mesto;

import javax.swing.*;


public class ZakladnaInfoFrame extends JFrame
{
    private JTextField textField_hracJmeno;
    private JTextField textField_hracPenize;
    private JTextField textField_hracRespekt;
    private JTextField textField_hracHledanost;
    private JPanel panel_zakladnaInfo;
    private JTextField textField_zakladnaPocetPoskoku;
    private JTextField textField_zakladnaMaxPocetposkoku;
    private JTextField textField_zakladnaLevel;
    private JButton button_back;


    public ZakladnaInfoFrame(Hrac hrac)
    {
        textField_hracJmeno.setText(hrac.getJmeno()); // upravit
        textField_hracPenize.setText(String.valueOf(hrac.getPenize()));
        textField_hracHledanost.setText(String.valueOf(hrac.getHledanost()));
        textField_hracRespekt.setText(String.valueOf(hrac.getRespekt()));
        textField_zakladnaLevel.setText(String.valueOf(hrac.getZakladna().getLevel()));
        textField_zakladnaMaxPocetposkoku.setText(String.valueOf(hrac.getZakladna().getMaximumPoskoku()));
        textField_zakladnaPocetPoskoku.setText(String.valueOf(hrac.getZakladna().getPocetposkokuNaZakladni()));

        button_back.addActionListener(e -> this.dispose());

    }



    public JPanel getPanel_zakladnaInfo()
    {
        return panel_zakladnaInfo;
    }
}


