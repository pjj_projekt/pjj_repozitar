package cz.mendelu.pjj.projekt.frame;

import cz.mendelu.pjj.projekt.model.Hrac;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ZakladnaFrame
{
    private JPanel panel_zakladna;
    private JButton button_info;
    private JButton button_akce;
    private JButton button_odejit;


    private JFrame jframe;
   // private Hrac hrac;




    public ZakladnaFrame(JFrame jframe)

    {
        this.jframe = jframe;

        button_info.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });
        button_akce.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {

            }
        });
        button_odejit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                jframe.dispose();
            }
        });
    }

    public JPanel getPanel_zakladna()
    {
        return panel_zakladna;
    }
}
