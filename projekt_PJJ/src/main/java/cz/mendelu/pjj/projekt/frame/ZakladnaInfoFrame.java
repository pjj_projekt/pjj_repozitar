package cz.mendelu.pjj.projekt.frame;

import javax.swing.*;

public class ZakladnaInfoFrame
{
    private JTextField textField_hracJmeno;
    private JTextField textField_hracPenize;
    private JTextField textField_hracRespekt;
    private JTextField textField_hracHledanost;
    private JPanel panel_zakladnaInfo;
    private JTextField textField_zakladnaPocetPoskoku;
    private JTextField textField_zakladnaMaxPocetposkoku;
    private JTextField textField_zakladnaLevel;
}
