package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.frame.Banka.BankaFrame;
import cz.mendelu.pjj.projekt.model.Banka;
import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import greenfoot.Actor;
import greenfoot.Greenfoot;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class BankaActor extends Actor
{
    private final Banka banka;
    private Hrac hrac;
    private City city;


    public BankaActor(City city, Hrac hrac)
    {
        //this.banka = banka;
        this.banka = city.getBanka();
        this.hrac = hrac;
        //this.hrac = city.getHrac();
        this.city = city;
        setImage("images/bank-icon.png");
    }

    @Override
    public void act()
    {
        if (Greenfoot.mouseClicked(this))
        {
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {

                    JFrame jframe = new JFrame();
                    jframe.setVisible(true);
                   // jframe.setContentPane(new BankaFrame(jframe, city).getPanel_banka());
                    jframe.setContentPane(new BankaFrame(jframe, city, hrac).getPanel_banka());
                    jframe.setSize(650, 350);


                    jframe.addWindowFocusListener(new WindowAdapter()
                    {
                        @Override
                        public void windowClosing(WindowEvent e)
                        {
                            super.windowClosing(e);

                        }

                    });

                }
            });
        }
    }
}
