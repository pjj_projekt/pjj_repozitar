package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.frame.Bar.BarFrame;
import cz.mendelu.pjj.projekt.model.Bar;
import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import greenfoot.Actor;
import greenfoot.Greenfoot;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class BarActor extends Actor
{
    private final Bar bar;
    private Hrac hrac;
    private City city;

    public BarActor(City city, Hrac hrac)
    {
        this.bar = city.getBar();
        this.hrac = hrac;
        //this.hrac = city.getHrac();
        this.city = city;
        setImage("images/pub-icon.png");
    }


    @Override
    public void act()
    {
        if (Greenfoot.mouseClicked(this))
        {
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {

                    JFrame jframe = new JFrame();
                    jframe.setVisible(true);
                    jframe.setContentPane(new BarFrame(jframe, city, hrac).getPanel_bar());
                    jframe.setSize(650, 350);


                    jframe.addWindowFocusListener(new WindowAdapter()
                    {
                        @Override
                        public void windowClosing(WindowEvent e)
                        {
                            super.windowClosing(e);

                        }

                    });

                }
            });

        }
    }

}
