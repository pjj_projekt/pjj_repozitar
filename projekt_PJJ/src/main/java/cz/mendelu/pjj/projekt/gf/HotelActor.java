package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.frame.Hotel.HotelFrame;
import cz.mendelu.pjj.projekt.model.Hotel;
import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import greenfoot.Actor;
import greenfoot.Greenfoot;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;



public class HotelActor extends Actor
{

    private final Hotel hotel;
    private Hrac hrac;
    private City city;





    public HotelActor(City city, Hrac hrac)
    {

        this.hrac = hrac;
        //this.hrac = city.getHrac();
        this.city = city;
        this.hotel = city.getHotel();
        setImage("images/hotel-icon.png");
    }


    @Override
    public void act()
    {
        if (Greenfoot.mouseClicked(this))
        {
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {

                    JFrame jframe = new JFrame();
                    jframe.setVisible(true);
                    jframe.setContentPane(new HotelFrame(jframe, city, hrac).getPanel_hotel());
                    jframe.setSize(650, 350);


                    jframe.addWindowFocusListener(new WindowAdapter()
                    {
                        @Override
                        public void windowClosing(WindowEvent e)
                        {
                            super.windowClosing(e);

                        }

                    });

                }
            });

        }
    }

}
