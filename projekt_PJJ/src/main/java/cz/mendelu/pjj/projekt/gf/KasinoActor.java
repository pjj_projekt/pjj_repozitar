package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.frame.Kasino.KasinoFrame;
import cz.mendelu.pjj.projekt.model.Hrac;
import cz.mendelu.pjj.projekt.model.Kasino;
import cz.mendelu.pjj.projekt.model.City;
import greenfoot.Actor;
import greenfoot.Greenfoot;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class KasinoActor extends Actor
{
    private final Kasino kasino;
    private Hrac hrac;
    private City city;


    public KasinoActor(City city, Hrac hrac)
    {
        this.kasino = city.getKasino();
        this.hrac = hrac;
        //this.hrac = city.getHrac();
        this.city = city;
        setImage("images/casino-icon.png");
    }


    @Override
    public void act()
    {
        if (Greenfoot.mouseClicked(this))
        {
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {

                    JFrame jframe = new JFrame();
                    jframe.setVisible(true);
                    jframe.setContentPane(new KasinoFrame(jframe, city, hrac).getPanel_kasino());
                    jframe.setSize(650, 350);


                    jframe.addWindowFocusListener(new WindowAdapter()
                    {
                        @Override
                        public void windowClosing(WindowEvent e)
                        {
                            super.windowClosing(e);

                        }

                    });

                }
            });

        }
    }
}
