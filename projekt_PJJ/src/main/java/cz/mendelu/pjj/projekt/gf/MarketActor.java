package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.frame.Market.MarketFrame;
import cz.mendelu.pjj.projekt.model.Market;
import cz.mendelu.pjj.projekt.model.City;
import greenfoot.Actor;
import greenfoot.Greenfoot;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class MarketActor extends Actor
{
    private final Market market;


    public MarketActor(City city)
    {
        this.market = city.getMarket();
        setImage("images/market-icon.png");
    }


    @Override
    public void act()
    {
        if (Greenfoot.mouseClicked(this))
        {
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {

                    JFrame jframe = new JFrame();
                    jframe.setVisible(true);
                    jframe.setContentPane(new MarketFrame(jframe).getPanel_market());
                    jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    jframe.setSize(650, 350);


                    jframe.addWindowFocusListener(new WindowAdapter()
                    {
                        @Override
                        public void windowClosing(WindowEvent e)
                        {
                            super.windowClosing(e);

                        }

                    });

                }
            });

        }
    }
}
