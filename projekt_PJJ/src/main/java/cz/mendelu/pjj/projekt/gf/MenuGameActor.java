package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.frame.MenuGame;
import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import greenfoot.Actor;
import greenfoot.Greenfoot;
//import greenfoot.City;


public class MenuGameActor extends Actor
{

    private final MenuGame menuGame;

    /*public MenuGameActor(MenuGame menuGame)
    {
        this.menuGame = menuGame;
        setImage("images/menu-icon.png");
    }*/

    //public MenuGameActor(City city/** /, Mesto mesto/**/)
    public MenuGameActor(City city, Hrac hrac)
    {
        setImage("images/menu-icon2.png");
        menuGame = new MenuGame(city, hrac/** /, mesto/**/);
    }

    @Override
    public void act()
    {
        if (Greenfoot.mouseClicked(this))
        {
            menuGame.openMenu();

        }
    }

}
