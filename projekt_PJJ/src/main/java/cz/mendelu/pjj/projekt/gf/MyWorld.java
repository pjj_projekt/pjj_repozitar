package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.model.*;
import greenfoot.Actor;

import java.util.List;


//public class MyWorld extends City
public class MyWorld extends greenfoot.World
{
    private static final int WORLD_WIDTH = 1500;
    private static final int WORLD_HEIGHT = 1000;
    private static final int WORLD_CELLSIZE = 1;

    private static final String playersName = "Pokusny";

    private final Hrac hrac;
    private final City city;

    public MyWorld()
    {
        //this(new City(WORLD_WIDTH, WORLD_HEIGHT),
            //    new Hrac(playersName));

        //Hrac hrac = new Hrac(playersName);
        this(new City(WORLD_WIDTH, WORLD_HEIGHT), new Hrac(playersName));





    }


    public MyWorld(City city, Hrac hrac )
    {
        super(city.getWidth(), city.getHeight(), WORLD_CELLSIZE);
        this.city = city;
        this.hrac = hrac;

        setBackground("images/map.png");


        addObject(new MenuGameActor(city, hrac),50,50);





        //addObject(new ZakladnaActor(city),185, 165);
        addObject(new ZakladnaActor(hrac), 185, 165);
       // addObject(new BankaActor(city),1250, 170);
        addObject(new BankaActor(city, hrac),1250, 170);
        addObject(new HotelActor(city, hrac),830,150);
        addObject(new BarActor(city, hrac), 1350, 360);
        addObject(new PolicejniStaniceActor(city, hrac), 950, 730);
        addObject(new KasinoActor(city, hrac),820, 780);
        addObject(new MarketActor(city),570, 360);

    }



    ///////
    public void update()
    {
        List<Actor> Actors = getObjects(Actor.class);
        for(int x = 0; x < city.getWidth(); x++)
        {
            for(int y = 0; y < city.getHeight(); y++)
            {

            }
        }
    }



} //toto "}" pripadne odebrat

