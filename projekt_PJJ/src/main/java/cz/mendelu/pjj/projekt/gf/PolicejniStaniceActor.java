package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.frame.PolicejniStanice.PolicejniStaniceFrame;
import cz.mendelu.pjj.projekt.model.Hrac;
import cz.mendelu.pjj.projekt.model.PolicejniStanice;
import cz.mendelu.pjj.projekt.model.City;
import greenfoot.Actor;
import greenfoot.Greenfoot;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class PolicejniStaniceActor extends Actor
{
    private final PolicejniStanice policejniStanice;
    private Hrac hrac;
    private City city;



    public PolicejniStaniceActor(City city, Hrac hrac)
    {
        this.policejniStanice = city.getPolicejniStanice();
        this.hrac = hrac;
        //this.hrac = city.getHrac();
        this.city = city;
        setImage("images/police-icon.png");
    }

    @Override
    public void act()
    {
        if (Greenfoot.mouseClicked(this))
        {
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {

                    JFrame jframe = new JFrame();
                    jframe.setVisible(true);
                    jframe.setContentPane(new PolicejniStaniceFrame(jframe, city, hrac).getPanel_policejniStanice());
                    jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    jframe.setSize(650, 350);


                    jframe.addWindowFocusListener(new WindowAdapter()
                    {
                        @Override
                        public void windowClosing(WindowEvent e)
                        {
                            super.windowClosing(e);

                        }

                    });

                }
            });

        }
    }
}
