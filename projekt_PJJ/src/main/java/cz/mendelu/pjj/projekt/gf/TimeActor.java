package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.model.Timer;
import greenfoot.Actor;
import greenfoot.GreenfootImage;

import java.awt.*;

public class TimeActor extends Actor {

    private final Timer timer;

    public TimeActor(Timer timer) {
        this.timer = timer;
        updateLabel();
    }

    @Override
    public void act() {
        timer.incTime();
        updateLabel();
    }

    private void updateLabel() {
        String label = "Time: " + timer.getValue();
        GreenfootImage image = new GreenfootImage(
                label,
                30,
                Color.BLUE,
                Color.WHITE
        );
        setImage(image);
    }
}
