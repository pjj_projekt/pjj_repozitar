package cz.mendelu.pjj.projekt.gf;

import cz.mendelu.pjj.projekt.frame.Zakladna.ZakladnaFrame;
import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import cz.mendelu.pjj.projekt.model.Zakladna;
import greenfoot.Actor;
import greenfoot.Greenfoot;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ZakladnaActor extends Actor
{
    private final Zakladna zakladna;
    private Hrac hrac;


    //public ZakladnaActor(City city)
    public ZakladnaActor(Hrac hrac)
    {
        //this.zakladna = zakladna;
        //this.zakladna = city.getHrac().getZakladna();
        //this.hrac = city.getHrac();
        this.hrac = hrac;
        this.zakladna = hrac.getZakladna();

        setImage("images/base-icon.png");
    }

    @Override
    public void act()
    {
        if (Greenfoot.mouseClicked(this))
        {

            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    JFrame jframe = new JFrame();
                    jframe.setVisible(true);
                    jframe.setContentPane(new ZakladnaFrame(jframe, hrac).getPanel_zakladna());
                    //jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    jframe.setSize(650,350);



                    jframe.addWindowFocusListener(new WindowAdapter()
                    {
                        @Override
                        public void windowClosing(WindowEvent e)
                        {
                            super.windowClosing(e);
                        }
                    });


                }
            });



        }
    }
}