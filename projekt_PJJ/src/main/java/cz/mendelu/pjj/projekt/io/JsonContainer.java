package cz.mendelu.pjj.projekt.io;

import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;

public class JsonContainer
{
    private City city;
    private Hrac hrac;

    public JsonContainer(City city, Hrac hrac)
    {
        this.city = city;
        this.hrac = hrac;
    }

    public City getCity()
    {
        return city;
    }

    public Hrac getHrac()
    {
        return hrac;
    }
}
