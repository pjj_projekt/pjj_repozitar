package cz.mendelu.pjj.projekt.io;


import com.google.gson.*;
import cz.mendelu.pjj.projekt.model.City;
import cz.mendelu.pjj.projekt.model.Hrac;
import cz.mendelu.pjj.projekt.model.Position;

import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;





public class JsonLibrary
{


    private JsonLibrary() {}

    private static class MapSerializer implements JsonSerializer<Map<Position, String>>, JsonDeserializer<Map<Position, String>> {

        @Override
        public Map<Position, String> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonArray jsonArray = (JsonArray) json;
            Map<Position, String> map = new HashMap<>();
            jsonArray.forEach(e -> {
                JsonObject jsonObject = (JsonObject) e;
                int x = jsonObject.get("x").getAsInt();
                int y = jsonObject.get("y").getAsInt();
                String t = jsonObject.get("t").getAsString();
                map.put(new Position(x, y), t);
            });
            return map;
        }

        @Override
        public JsonElement serialize(Map<Position, String> src, Type typeOfSrc, JsonSerializationContext context) {
            JsonArray result = new JsonArray();
            src.forEach((p, s) -> {
                JsonObject item = new JsonObject();
                item.addProperty("x", p.x);
                item.addProperty("y", p.y);
                item.addProperty("t", s);
                result.add(item);
            });
            return result;
        }
    }



    private static Gson GSON;
    static {
        GsonBuilder gb = new GsonBuilder();
        gb.registerTypeAdapter(Map.class, new MapSerializer());
        GSON = gb.create();
    }

    public static void save(City city, Hrac hrac, Writer writer) {
        GSON.toJson(new JsonContainer(city, hrac), writer);
    }

    public static JsonContainer load(Reader reader) {
        return GSON.fromJson(reader, JsonContainer.class);
    }

}
