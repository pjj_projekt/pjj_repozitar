package cz.mendelu.pjj.projekt.model;

import java.io.Serializable;
import java.util.*;

public class Banka extends Building implements Serializable
{

	private LinkedList<Ochranka> ochranka = new LinkedList<>();
	private int maxPocetOchrankaru;
	//private SortedSet<Hrac> dluznici = new TreeSet<>();
	private Set<Hrac> dluznici = new HashSet<>();
	private double urok;

	/**
	 * 
	 * @param nazev
	 * @param pocetLidi
	 * @param cena
	 */
	public Banka(String nazev, int pocetLidi, double cena, int level, int maxPocetOchrankaru)
	{
		super(nazev, pocetLidi, cena, level);
		this.maxPocetOchrankaru = maxPocetOchrankaru;
		this.zisk = 1000; // upravit
		this.urok = 25; // upravit

		//this.ochranka.clear();

	}

	/**
	 * 
	 * @param ochranka
	 */
	public void najmoutOchranku(Ochranka ochranka)
	{
		if (this.ochranka.size() < maxPocetOchrankaru)
		{
			this.ochranka.addLast(ochranka);
			// neco jako
			// hrac.setmoney(-ochranka.getVyplata());
			// nebo v kazdem kole
			// hrac.setmoney(-ochranka.getvyplata());
			// ===>> nebo jenom to odecist ze zisku budovi -> napr. zisk muze byt zaporny a hraci prichazi o penize
		}
		else
		{
			System.out.println("Nelze najmout vice ochrankaru");
		}
	}

	/**
	 * 
	 * @param kolik
	 */
	public void vzitSiPujcku(Hrac kdo, double kolik)
	{
		// hrac.setMoney(kolik);
		// ulozit do nejaky promenne dluh + nejaky pocitadlo kol, ktere bude zvetsovat urok
		dluznici.add(kdo);
		kdo.setPenize(kdo.getPenize() + kolik);

	}

	public void splatitPujcku(Hrac kdo, double kolik)
	{
		if(dluznici.contains(kdo))
		{
			kdo.setDluh(-kolik); // skontorlovat jestli spravne odecte dluh
		}
		// nejaka prommenna -> dluh = 0;
	}

	public int getPocetOchrankaru()
	{
		return ochranka.size();
	}

	public int getMaxPocetOchrankaru()
	{
		return this.maxPocetOchrankaru;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setMaxPocetOchrankaru(int okolik)
	{
		this.maxPocetOchrankaru += okolik;
	}

	public double getZisk() // mozno upravit + zkontrolovat hodnotu pom + zkontrolovat return pro kupovane
	{

		double pom = 0;
		for (int i = 0; i < ochranka.size(); i++)
		{
			pom += ochranka.get(i).getPlat();
		}

		if(okupovane == false)
		{
			return level * zisk - pom;
		}
		else
		{
			return level * zisk - pom - vypalne;
		}
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setZisk(double okolik)
	{
		zisk += okolik; // upravit
	}


	public Set<Hrac> getDluznici()
	{
		return dluznici;
	}

}