package cz.mendelu.pjj.projekt.model;

import java.io.Serializable;

public class Bar extends Building implements Serializable
{

	private int pocetStamgastu;
	private int maxPocetStamgastu;

	/**
	 * 
	 * @param pocetStamgastu
	 * @param nazev
	 * @param pocetLidi
	 * @param cena
	 * @param level
	 */
	public Bar(int pocetStamgastu, int maxPocetStamgastu, String nazev, int pocetLidi, double cena, int level)
	{
		super(nazev, pocetLidi, cena, level);
		this.pocetStamgastu = pocetStamgastu;
		this.maxPocetStamgastu = maxPocetStamgastu;
	}

	public int getPocetStamgastu()
	{
		return this.pocetStamgastu;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setPocetStamgastu(int okolik)
	{
		this.pocetStamgastu += okolik;
	}

	public double getZisk()
	{
		if (okupovane == false)
		{
			return level * zisk * pocetStamgastu; // upravit
		}
		else
		{
			return level * zisk * pocetStamgastu - vypalne;
		}
	}

	public int getMaxPocetStamgastu()
	{
		return maxPocetStamgastu;
	}

	public void setMaxPocetStamgastu(int okolik)
	{
		this.maxPocetStamgastu += okolik;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setZisk(double okolik)
	{
		zisk += okolik; // upravit
	}

}