package cz.mendelu.pjj.projekt.model;

import java.io.Serializable;
import java.util.LinkedList;

public abstract class Building implements Serializable
{

	// koupit, prodat -> asi udelat v hracovi

	protected double cena;
	protected double zisk;
	protected LinkedList<Poskok> lidi;
	protected int maxPocetLidi; // poskoku
	protected String nazev;
	protected int stavBudovy; // chceme to vubec? jestli jo, tak jak s tim budeme pracovat -> kde to chceme zahrnout a pouzit
	protected int level;
	protected boolean podNadvladou; // ===>>> jak zjistit pod koho nadvladou je budova? --> mozna >> vvvv
	protected Hrac vlastnik;
	protected boolean okupovane;	// ===>>> jak zjistit kdo okupuje budovu? --> mozna >> vvvv
	protected Hrac okupant;
	protected double vypalne; // celkove vypalne z okupovane
	protected LinkedList<Poskok> vypalnici;

	/**
	 * 
	 * @param nazev
	 * @param maxPocetLidi
	 * @param cena
	 * @param level
	 */
	public Building(String nazev, int maxPocetLidi, double cena, int level)
	{
		this.nazev = nazev;
		this.maxPocetLidi = maxPocetLidi;
		this.cena = cena;
		this.level = level;
		this.podNadvladou = false;
		this.okupovane = false;
		this.vlastnik = null;
		this.okupant = null;
	}

	/**
	 *
	 //* @param zaKolik
	 */
/*	public void koupit(Hrac kdo/*, double zaKolik* /)
	{
		// neco s cenou
		// napr. -> hrac.setMoney(-(cena-hrac.getRespekt()*5));
		kdo.setPenize(-(cena - kdo.getRespekt() * 5));
		this.vlastnik = kdo;
		podNadvladou = true;
	}
*/
	/**
	 * 
	 //* @param zaKolik
	 */
/*	public void prodat(/*double zaKolil* /) //mozna jeste pridat metodu prodat(Hrac komu) -> ale funkci tohle metody asi resi koupit
	{
		// neco s cenou
		vlastnik.setPenize(cena);
		vlastnik = null;
		podNadvladou = false;

	}
*/

	/**
	 * 
	 * @param clovek
	 */
	public void podporit(Poskok clovek)
	{
		this.lidi.addLast(clovek);
	}

	public void okupovat(Hrac kdo)
	{
		this.okupovane = true;
		this.okupant = kdo;

		// mozna -> double okupace = zisk/200; hrac.setMoney(okupace);
		// vypalne += okupace; --> mozna udelat jenom vypalne (odstranit double okupace) a okupovat muze jen jeden hrac
		// TODO - implement Building.okupovat
		throw new UnsupportedOperationException();

	}

	public void zastavitOkupaci()
	{
		this.okupovane = false;
		this.okupant = null;
		// udelat jeste neco
		// jako linkedList vypalnici; vypalnici.clear();
	}

	public void vyplacetVypalne(Hrac komu) // tahle metoda musi bezet kazde kolo(turn)
	{
		if (okupant == null)
		{
			// mozna nastavit, ze vypalne se vyplaci vlastnikovi, ale spis ne; asi jenom nedelat nic
		}
		else
		{
			komu.setPenize(vypalne);
		}

	}

	public void vypalit() // tahle metoda musi bezet kazde kolo
	{
		// promyslet, co chceme, aby to delalo -> napr. nastavi zisk = zisk/ 500; level -= 2; lidi.clear(); ....
		vlastnik.setPenize(getZisk()); // otestovat jestli to yplaci spravnou sumu
		// TODO - implement Building.vypalit
		throw new UnsupportedOperationException();
	}

	public String getNazev()
	{
		return this.nazev;
	}

	public double getCena()
	{
		return this.cena * level; // upravit
	}

	public int getMaxPocetLidi()
	{
		return this.maxPocetLidi;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setMaxPocetLidi(int okolik)
	{
		this.maxPocetLidi += okolik;
	}

	/**
	 * 
	 * @param okolik
	 */
	public abstract void setZisk(double okolik);

	public abstract double getZisk();

	// tohle pouzivat na vyplaceni zisku z budovy
	// public abstract double getFinalZisk(); -> mozna to resit jenom v getZisk(); >>> asi se to tam uz resi(zkontroluj to)

	/**
	 * 
	 * @param okolik
	 */
	public void setCena(double okolik)
	{
		this.cena += okolik;
	}

	public int getLevel()
	{
		return this.level;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setLevel(int okolik)
	{
		this.level += okolik;
	}

	public int getStavBudovy()
	{
		return this.stavBudovy;
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setStavBudovy(int oKolik)
	{
		this.stavBudovy += oKolik;
	}

	public double getVypalne()
	{
		return vypalne;
	}

	public void setVypalne()
	{
		// TODO - implement Building.vypalit
		throw new UnsupportedOperationException();
	}


	public double getCenaZaLevel()
	{
		return level * 300; // upravit
	}

	public Hrac getVlastnik()
	{
		return vlastnik;
	}

	public void setVlastnik(Hrac vlastnik)
	{
		this.vlastnik = vlastnik;
	}

	public boolean getPodNadvladou()
	{
		return podNadvladou;
	}

	public void setPodNadvladou(boolean podNadvladou)
	{
		this.podNadvladou = podNadvladou;
	}
}