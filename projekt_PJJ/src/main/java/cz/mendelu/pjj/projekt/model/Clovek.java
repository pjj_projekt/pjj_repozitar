package cz.mendelu.pjj.projekt.model;

public abstract class Clovek
{

	protected int zivoty;
	protected Zbran zbran;
	protected Zbroj zbroj;
	protected int presnost;
	protected int kryti;

	/**
	 * 
	 * @param zivoty
	 * @param zbran
	 * @param presnost
	 * @param kryti
	 */
	public Clovek(int zivoty, Zbran zbran, Zbroj zbroj, int presnost, int kryti)
	{
		this.zivoty = zivoty;
		this.zbran = zbran;
		this.zbroj = zbroj;
		this.presnost = presnost;
		this.kryti = kryti;
	}

	public int getZivoty()
	{
		return this.zivoty;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setZivoty(int okolik)
	{
		this.zivoty += okolik;
	}

	public int getUtok()
	{
		return zbran.getUtok() * presnost; // upravit
	}

	public int getPresnost()
	{
		return this.presnost + zbran.getPresnostZbrane(); // upravit
	}

	public int getKriti()
	{
		return this.kryti + zbroj.getObrana();
	}

	/**
	 * 
	 * @param zbran
	 */
	public void setZbran(Zbran zbran)
	{
		this.zbran = zbran;
	}

	/**
	 * 
	 * @param zbroj
	 */
	public void setZbroj(Zbroj zbroj)
	{
		this.zbroj = zbroj;
	}


}