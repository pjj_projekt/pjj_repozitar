package cz.mendelu.pjj.projekt.model;

public class Droga extends Surovina
{

	private int sila;

	/**
	 * 
	 * @param sila
	 */
	public Droga(int sila, double cena, int velikostPolozky)
	{
		super(cena, velikostPolozky);
		this.sila = sila;
		this.typSuroviny = "droga";
	}

	public double getCena()
	{
		return cena * sila; // upravit pocitani
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setCena(double oKolik)
	{
		this.cena += oKolik;
	}

	public int getSila()
	{
		return this.sila;
	}

}