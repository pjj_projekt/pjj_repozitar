package cz.mendelu.pjj.projekt.model;

import java.io.Serializable;

public class Hotel extends Building implements Serializable
{

	private int pocetApartmanu;

	/**
	 * 
	 * @param pocetApartmanu
	 * @param nazev
	 * @param pocetLidi
	 * @param cena
	 * @param level
	 */
	public Hotel(int pocetApartmanu, String nazev, int pocetLidi, double cena, int level)
	{
		super(nazev, pocetLidi, cena, level);
		this.pocetApartmanu = pocetApartmanu;
	}

	public int getPocetApartmanu()	// mozna zbytecne
	{
		return this.pocetApartmanu;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setPocetApartmanu(int okolik) // tohle zrejme zahrnout nejakym zpusobem do upgradeBudovy();
	{
		this.pocetApartmanu = okolik;
	}

	public double getZisk()
	{
		if(okupovane == false)
		{
			return level * zisk * pocetApartmanu; // upravit
		}
		else
		{
			return level * zisk * pocetApartmanu - vypalne;
		}

	}

	/**
	 * 
	 * @param okolik
	 */
	public void setZisk(double okolik)
	{
		zisk += okolik; // upravit
	}

}