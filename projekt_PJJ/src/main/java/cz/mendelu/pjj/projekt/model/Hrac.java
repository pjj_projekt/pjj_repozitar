package cz.mendelu.pjj.projekt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Hrac implements Serializable
{

	private String jmeno;
	private int respekt;
	private double penize;
	private Zakladna zakladna;
	private int pocetAkciZaKolo;
	private int zbyvajiciAkce;
	private int hledanost;
	private double dluh; // pro banku
	private List<Building> nehnutelnosti = new ArrayList<>();



	private int konst = 5; // konstanta pro pocitani pri praci s respektem : koupitBudovu



	// uprava na budovy -> kolekce >> nehnutenosti




	/**
	 * 
	 * @param jmeno
	 */
	public Hrac(String jmeno)
	//public Hrac(String jmeno, Zakladna zakladna) // uprava kvoli greenfoot
	{
		this.jmeno = jmeno;
		this.respekt = 10; // upravit
		this.zakladna = new Zakladna(); // >> inak -> neviem nasadit do greenfoot; mozno uz to ide
		this.pocetAkciZaKolo = 5; // upravit
		this.zbyvajiciAkce = pocetAkciZaKolo;
		this.penize = 2000; // upravit
		this.hledanost = 0;
		//this.zakladna = zakladna; // pridane kvoli greenfoot



	}

	public int getRespekt()
	{
		return this.respekt;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setRespekt(int okolik)
	{
		this.respekt += okolik;
	}

	public double getPenize()
	{
		return this.penize;
	}

	/**
	 * 
	 * @param budova
	 */
	public void interagujSbudovou(Building budova) // asi to radeji udelat v Building -->> public void interagujShracem(Hrac hrac);
	{
		// >>> tato metoda asi nie je potrebna -> riesi to Frame
		// tady pridat asi nejaky command
		// TODO - implement Hrac.interagujSbudovou
		throw new UnsupportedOperationException();
	}

	public int getPocetAkciZaKolo()
	{
		return this.pocetAkciZaKolo;
	}

	/**
	 * 
	 * @param kolik
	 */
	public void setPocetAkciZaKolo(int kolik) // mozna okolik
	{
		this.pocetAkciZaKolo = kolik;
	}

	public int getZbyvajiciAkce()
	{
		return this.zbyvajiciAkce;
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setZbyvajiciAkce(int oKolik)// zkontrolovat znaminka -> jestli +=okolik nebo -=okolik
	{
		zbyvajiciAkce -= oKolik;
	}


	public void setPenize(double penize)
	{
		this.penize = penize;
	}

	public  Zakladna getZakladna()
	{
		return this.zakladna;

	}


	public int getHledanost()
	{
		return hledanost;
	}

	public void setHledanost(int okolik)
	{
		this.hledanost += okolik;
	}


	public String getJmeno()
	{
		return jmeno;
	}


	public double getDluh()
	{
		return dluh;
	}

	public void setDluh(double okolik)
	{
		this.dluh += okolik;
	}



	public void koupitBudovu(Building budova)
	{
		if(budova.getVlastnik() == null)
		{
			penize -= (budova.getCena() - respekt * konst);
			budova.setVlastnik(this);
			budova.setPodNadvladou(true);
			nehnutelnosti.add(budova);
		}
		// else -> frame, ze to nejde
	}

	public void  prodatBudovu(Building budova)
	{
		penize += (budova.getCena() + respekt * konst);
		budova.setVlastnik(null);
		budova.setPodNadvladou(false);
		nehnutelnosti.remove(budova);
	}


	// je to v bance; tohle byl pokus
	/*
	public void vzitSiPujcku(Banka banka, double kolik)
	{
		banka.getDluznici().add(this);
		penize += kolik;
	}

*/




}