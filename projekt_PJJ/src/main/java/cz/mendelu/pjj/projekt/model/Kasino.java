package cz.mendelu.pjj.projekt.model;

import java.io.Serializable;

public class Kasino extends Building implements Serializable
{

	private int vybavenost;

	/**
	 * 
	 * @param vybavenost
	 * @param nazev
	 * @param pocetLidi
	 * @param cena
	 * @param level
	 */
	public Kasino(int vybavenost, String nazev, int pocetLidi, double cena, int level)
	{
		super(nazev, pocetLidi, cena, level);
		this.vybavenost = vybavenost;
	}

	public int getVybavenost()
	{
		return this.vybavenost;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setVybavenost(int okolik)
	{
		this.vybavenost += okolik;
	}

	public double getZisk()
	{
		if(okupovane == false)
		{
			return level * zisk * vybavenost; // upravit
		}
		else
		{
			return level * zisk* vybavenost - vypalne;
		}

	}

	/**
	 * 
	 * @param okolik
	 */
	public void setZisk(double okolik)
	{
		zisk += okolik;
	}

}