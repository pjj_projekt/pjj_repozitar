package cz.mendelu.pjj.projekt.model;



public class Mesto
{
    private static Mesto mesto = new Mesto();


    private Banka banka;
    private Hotel hotel;
    private Sklad sklad;
    private Zakladna zakladna;
    private Hrac hrac1;
    private PolicejniStanice policejniStanice;
    private Bar bar;
    private Kasino kasino;
    private Market market;

   // private static Sklad sklad;
    // static Zakladna zakladna = new Zakladna(sklad);
   // private static Hrac hrac1 = new Hrac("Hrac1", zakladna);





    private Mesto()
    {
        // pridat tridy
        banka = new Banka("Zatim jen banka1", 0, 4000, 1, 10);
        hotel = new Hotel(10,"zatim jen hotel1", 0, 2500, 1);
        sklad = new Sklad();
        //zakladna = new Zakladna(sklad);
       // hrac1 = new Hrac("Hrac1", zakladna);
        //hrac1.setZakladna(zakladna);

       // zakladna = new Zakladna();
        //zakladna.setSklad(sklad);
        policejniStanice = new PolicejniStanice();
        bar = new Bar(10,20, "Zatim jen bar1",0,2000,1);
        kasino = new Kasino(10,"Zatim jen kasino1", 0,2500,1);
        market = new Market("Zatim jen market1", 40);
        //hrac1 = new Hrac("Hrac1");






    }


    public static Mesto getMesto()
    {
        return mesto;
    }

    public Banka getBanka()
    {
        return banka;
    }

    public Hotel getHotel()
    {
        return hotel;
    }

    public Zakladna getZakladna()
    {
        return zakladna;
    }

    public PolicejniStanice getPolicejniStanice()
    {
        return policejniStanice;
    }


    public Bar getBar()
    {
        return bar;
    }

    public Kasino getKasino()
    {
        return kasino;
    }

    public Market getMarket()
    {
        return market;
    }

    public Hrac getHrac1()
    {
        return hrac1;
    }
}
