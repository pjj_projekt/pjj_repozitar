package cz.mendelu.pjj.projekt.model;

public class Ochranka extends Clovek
{

	private double plat;

	/**
	 * 
	 * @param plat
	 * @param zivoty
	 * @param zbran
	 * @param presnost
	 * @param kryti
	 */
	public Ochranka(double plat, int zivoty, Zbran zbran, Zbroj zbroj, int presnost, int kryti)
	{
		super(zivoty, zbran, zbroj, presnost, kryti);
		this.plat = plat;
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setPlat(double oKolik)
	{
		this.plat += oKolik;
	}

	public double getPlat()
	{
		return plat;
	}

}