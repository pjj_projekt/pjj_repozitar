package cz.mendelu.pjj.projekt.model;

import java.io.Serializable;
import java.util.LinkedList;



public class PolicejniStanice implements Serializable
{


	private static PolicejniStanice policejniStanice = new PolicejniStanice();


	private LinkedList<Policista> policisti = new LinkedList<>();

	public PolicejniStanice()
	{
		this.policisti.clear();
	}

	/**
	 * 
	 * @param kolik
	 */
	public void uplatit(double kolik, Hrac hrac)
	{
		// hrac.setMoney(-kolik);
		// + znizit nejakou hledanost nebo neco takoveho -> pridat hraci atribut hledanost
		// KONTROLOVAT zbyvajici pocet akci
		if (hrac.getZbyvajiciAkce() > 0)
		{
			hrac.setPenize(- (-hrac.getHledanost() * 500)); // upravit
			hrac.setHledanost(hrac.getHledanost()- hrac.getHledanost()); // nastavi na 0; mozna zmenit
			hrac.setZbyvajiciAkce(1); // POZOR -> poteba kontrolovat ZNAMIENKO >> +1 alebo -1 -->>> podla toho ako je to nastavene v hracovi
		}
		else
		{
			System.out.println("Nedostatek akci");
		}

	}

	public static PolicejniStanice getPolicejniStanice()
	{
		return policejniStanice;
	}


	// mozna pridat metody
	// pridej policitu
	// odeber policistu

}