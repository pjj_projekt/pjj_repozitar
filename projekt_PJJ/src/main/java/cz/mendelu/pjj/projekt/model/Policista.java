package cz.mendelu.pjj.projekt.model;

public class Policista extends Clovek
{

	private float uplatnost;

	/**
	 * 
	 * @param uplatnost
	 * @param zivoty
	 * @param zbran
	 * @param presnost
	 * @param kryti
	 */
	public Policista(float uplatnost, int zivoty, Zbran zbran, Zbroj zbroj, int presnost, int kryti)
	{
		super(zivoty, zbran, zbroj, presnost, kryti);
		this.uplatnost = uplatnost;
	}

	public float getUplatnost()
	{
		return this.uplatnost;
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setUplatnost(float oKolik)
	{
		this.uplatnost += oKolik;
	}

}