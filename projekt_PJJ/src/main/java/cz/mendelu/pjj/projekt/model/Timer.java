package cz.mendelu.pjj.projekt.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Timer {

    public interface OnTimeListener {

        public void onTime(Timer timer);
    }

    public static class TimeEvent {

        private final OnTimeListener listener;
        private final int eventTime;
        private final Object tag;

        public TimeEvent(OnTimeListener listener, int eventTime, Object tag) {
            this.listener = listener;
            this.eventTime = eventTime;
            this.tag = tag;
        }

        public OnTimeListener getListener() {
            return listener;
        }

        public int getEventTime() {
            return eventTime;
        }

        public Object getTag() {
            return tag;
        }
    }

    private List<TimeEvent> timeEvents = new LinkedList<>();

    private int value;

    public void incTime() {
        value++;
        for (TimeEvent event : timeEvents) {
            if (event.eventTime == value) {
                event.listener.onTime(this);
                timeEvents.remove(event);
            }
        }
    }

    public int getValue() {
        return value;
    }

    public List<TimeEvent> getTimeEvents() {
        return Collections.unmodifiableList(timeEvents);
    }

    public void addOnTimeListener(Object tag, OnTimeListener listener, int time){
        timeEvents.add(new TimeEvent(listener, value + time, tag));
    }
}
