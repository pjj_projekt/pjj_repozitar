package cz.mendelu.pjj.projekt.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class World
{
    private final int width;
    private final int height;
    private Map<Position, Object> map = new HashMap<>();




    //objects
    private Banka banka;
    private Hotel hotel;
    private Sklad sklad;
    private Zakladna zakladna;
    private PolicejniStanice policejniStanice;
    private Bar bar;
    private Kasino kasino;
    private Market market;

    private Hrac hrac;


    //objects




    public World(int width, int height, Hrac hrac)
    {

        this.width = width;
        this.height = height;


        this.hrac = hrac;


        //objects
        banka = new Banka("Zatim jen banka1", 0, 4000, 1, 10);
        hotel = new Hotel(10,"zatim jen hotel1", 0, 2500, 1);
        //sklad = new Sklad();
        //zakladna = new Zakladna(sklad);



        // zakladna = new Zakladna();
        //zakladna.setSklad(sklad);
        policejniStanice = new PolicejniStanice();
        bar = new Bar(10,20, "Zatim jen bar1",0,2000,1);
        kasino = new Kasino(10,"Zatim jen kasino1", 0,2500,1);
        market = new Market("Zatim jen market1", 40);

        //objects
    }



    public Object addObjectAt(Object object, int x, int y)
    {
        map.put(new Position(x, y), object);
        return object;
    }

    public Object getObjectAt(int x, int y)
    {
        return map.get(new Position(x,y));
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }



    // objects

    public Banka getBanka()
    {
        return banka;
    }

    public Hotel getHotel()
    {
        return hotel;
    }

    public Zakladna getZakladna()
    {
        return zakladna;
    }

    public PolicejniStanice getPolicejniStanice()
    {
        return policejniStanice;
    }


    public Bar getBar()
    {
        return bar;
    }

    public Kasino getKasino()
    {
        return kasino;
    }

    public Market getMarket()
    {
        return market;
    }

    public Hrac getHrac()
    {
        return hrac;
    }


    // objects
}
