package cz.mendelu.pjj.projekt.model;

import java.io.Serializable;
import java.util.LinkedList;

public class Zakladna implements Serializable
{

	private LinkedList<Poskok> poskoci;
	private int maximumPoskoku;
	private LinkedList<Poskok> poskociNaZakladni;
	private int level;
	private Sklad sklad;

	// maximumPoskoku -> pridat asi metodu calculateMaxPoskoku();

	//public Zakladna(Sklad sklad)
	public Zakladna()
	{

		this.level = 1;
		this.maximumPoskoku = 10; // upravit -> pocitat nejako z repektu, ale na zacatku je to asi jedno
		this.poskoci = new LinkedList<Poskok>();
		this.poskociNaZakladni = new LinkedList<Poskok>();

		//this.sklad = sklad;
		this.sklad = new Sklad();
	}

	public int getMaximumPoskoku()
	{
		return this.maximumPoskoku;
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setMaximumPoskoku(int oKolik)
	{
		// tady to asi pocitat pomoci atributu 'respekt' u  hrace
		this.maximumPoskoku = oKolik;
	}

	/**
	 * 
	 * @param poskok
	 */
	public void najmoutPoskoka(Poskok poskok)
	{
		if(poskoci.size() < maximumPoskoku)
		{
			poskoci.addLast(poskok);
		}
		else
		{
			System.out.println("Nelze najmout vice poskoku.");
		}


	}

	/**
	 * 
	 * @param poskok
	 */
	public void propustitPoskoka(Poskok poskok)
	{
		poskoci.remove(poskok);
		// zkontrolovat

	}

	public Poskok getPoskokAt(int i)
	{
		return poskoci.get(i);
	}

	public  void seznamPoskoku()
	{
		for (int i = 0; i < poskoci.size(); i++)
		{
			System.out.println("Poskoci:");
			System.out.println(i + ") ");
			poskoci.get(i).printInfo();
		}
	}

	public int getLevel()
	{
		return this.level;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setLevel(int okolik)
	{
		level += okolik;
	}

	public Sklad getSklad()
	{
		return sklad;
	}


	// set kvoli greenfoot
	public void setSklad(Sklad sklad)
	{
		this.sklad = sklad;
	}

	public int getPocetposkokuNaZakladni()
	{
		return poskociNaZakladni.size();
	}
}