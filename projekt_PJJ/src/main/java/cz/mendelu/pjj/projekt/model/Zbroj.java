package cz.mendelu.pjj.projekt.model;

public class Zbroj
{

	private int obrana;

	/**
	 * 
	 * @param obrana
	 */
	public Zbroj(int obrana)
	{
		this.obrana = obrana;
	}

	public int getObrana()
	{
		return this.obrana;
	}

	/**
	 * 
	 * @param okolik
	 */
	public void setObrana(int okolik)
	{
		this.obrana = okolik;
	}

	public void upgradeZbroje()
	{
		// TODO - implement Zbroj.upgradeZbroje
		throw new UnsupportedOperationException();
	}

	public void printInfo()
	{
		System.out.println("obrana: " + obrana);
	}

}