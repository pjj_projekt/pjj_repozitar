package cz.mendelu.pjj.projekt.model;

public class Zlato extends Surovina
{

	private float hmotnost;

	/**
	 * 
	 * @param hmotnost
	 */
	public Zlato(float hmotnost, double cena, int velikostPolozky)
	{
		super(cena, velikostPolozky);
		this.hmotnost = hmotnost;
		this.typSuroviny = "zlato";
	}

	public double getCena()
	{
		return cena * hmotnost; // upravit pocitani
	}

	/**
	 * 
	 * @param oKolik
	 */
	public void setCena(double oKolik)
	{
		this.cena += oKolik;
	}

	public float getHmotnost()
	{
		return this.hmotnost;
	}

}